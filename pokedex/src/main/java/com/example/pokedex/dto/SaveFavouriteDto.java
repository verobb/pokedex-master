package com.example.pokedex.dto;

import lombok.Data;

@Data
public class SaveFavouriteDto {

	private Long idPokedex;
}
