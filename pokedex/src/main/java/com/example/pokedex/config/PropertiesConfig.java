package com.example.pokedex.config;

import java.io.File;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;

@Configuration
public class PropertiesConfig {
	
	@Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();

        //Include pokemon.propertiesl in file system optional (properties are too in application.properties)
        if(System.getProperty("os.name").contains("Windows")) {
            propertySourcesPlaceholderConfigurer.setLocations(
            		 new FileSystemResource("C:\\pokemonAPI_properties\\pokemon.properties"));                   
        }else {
            propertySourcesPlaceholderConfigurer.setLocations(
                    new FileSystemResource(File.separator+"opt"+File.separator+"tomcat"+File.separator+"pokemonAPI_properties"+File.separator+"pokemon.properties"));
        }
                      
        propertySourcesPlaceholderConfigurer.setIgnoreResourceNotFound(true);

        return propertySourcesPlaceholderConfigurer;

    }

}
