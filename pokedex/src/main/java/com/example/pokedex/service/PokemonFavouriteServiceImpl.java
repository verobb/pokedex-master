package com.example.pokedex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.example.pokedex.dao.MemberDao;
import com.example.pokedex.dao.PokemonDao;
import com.example.pokedex.dao.RelMemberPokemonFavDao;
import com.example.pokedex.model.Member;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.model.RelMemberPokemonFav;

@Service("pokemonFavouriteService")
public class PokemonFavouriteServiceImpl implements PokemonFavouriteService {


	@Autowired
	private PokemonDao pokemonDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private RelMemberPokemonFavDao relMemberPokemonFavDao;

	@Override
	public Pokemon saveFavourite(Long idPokedex, Long idMember) {
		
		RelMemberPokemonFav pokemonFav = new RelMemberPokemonFav();
		
		Member member = memberDao.getById(idMember);
		Pokemon pokemon = pokemonDao.getByIdPokedexOrderByIdPokedexAsc(idPokedex);
				
		try {
			if(member != null && pokemon != null) {
				pokemonFav.setMember(member);
				pokemonFav.setPokemon(pokemon);		

				relMemberPokemonFavDao.save(pokemonFav);
			}	
		} catch(DataIntegrityViolationException e) {
			
		}
			
		
		return pokemon;
	}
	
	@Override
	public boolean deleteFavourite (Long idPokedex, Long idMember) {
		
		RelMemberPokemonFav pokemonFav = relMemberPokemonFavDao.findBymemberIdAndPokemonIdPokedex(idMember, idPokedex);
		
		if(pokemonFav != null) {
			relMemberPokemonFavDao.delete(pokemonFav);
			return true;
		} else {
			return false;
		}
				
	}
	
	@Override
	public Boolean isFavourite(long idPokedex, Long idMember) {
		
		RelMemberPokemonFav pokemonFav = relMemberPokemonFavDao.findBymemberIdAndPokemonIdPokedex(idMember, idPokedex);	
		
		if(pokemonFav != null) {
			return true;
		}
		
		return false;
	}
}
