package com.example.pokedex.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "rel_member_pokemon_fav")
@javax.persistence.SequenceGenerator(name = "sequence", sequenceName = "seq_rel_member_pokemon_fav", allocationSize = 1)
@Getter @Setter @NoArgsConstructor
public class RelMemberPokemonFav extends DBEntity{	

	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity=Member.class)
	@JoinColumn(name = "id_member", nullable = false)
	private Member member;
	
	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity=Pokemon.class)
	@JoinColumn(name = "id_pokemon", nullable = false)
	private Pokemon pokemon;

}
