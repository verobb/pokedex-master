package com.example.pokedex.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pokemon_evolution")
@javax.persistence.SequenceGenerator(name = "sequence", sequenceName = "seq_pokemon_evolution", allocationSize = 1)
@Getter @Setter @NoArgsConstructor
public class PokemonEvolution extends DBEntity {

	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity=Pokemon.class)
	@JoinColumn(name = "id_pokemon", nullable = false)
	private Pokemon pokemon;
	
	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity=Pokemon.class)
	@JoinColumn(name = "id_pokemon_evolution", nullable = false)
	private Pokemon pokemonEvolution;
}
