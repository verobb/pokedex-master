package com.example.pokedex.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pokemon_type")
@javax.persistence.SequenceGenerator(name = "sequence", sequenceName = "seq_pokemon_type", allocationSize = 1)
@Getter @Setter @NoArgsConstructor
public class PokemonType extends DBEntity {

	
	private String type;

	
}

 