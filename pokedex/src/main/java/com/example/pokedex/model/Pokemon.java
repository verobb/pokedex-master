package com.example.pokedex.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.JoinColumn;


@Entity
@Table(name = "pokemon")
@javax.persistence.SequenceGenerator(name = "sequence", sequenceName = "seq_pokemon", allocationSize = 1)
@Getter @Setter @NoArgsConstructor
public class Pokemon extends DBEntity {

	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Long combatPoints;
	
	@Column(nullable = false)
	private Long healthPoints;
	
	@Column(nullable = false)
	private Float weightFrom;
	
	@Column(nullable = false)
	private Float weightTo;
	
	@Column(nullable = false)
	private Float heightFrom;
	
	@Column(nullable = false)
	private Float heightTo;
	
	@Column(nullable = false)
	private String imageUrl;
	
	@Column(nullable = false)
	private String soundUrl;
	
	@Column(nullable = false)
	private Long idPokedex;
	
	
	@ManyToMany( fetch = FetchType.EAGER)
	@JoinTable(name = "rel_pokemon_type", joinColumns = { @JoinColumn(name = "id_pokemon") }, inverseJoinColumns = { @JoinColumn(name = "id_pokemon_type") })
	private Set<PokemonType> types = new HashSet<PokemonType>(0);
	
	
	@ManyToMany( fetch = FetchType.EAGER)
	@JoinTable(name = "pokemon_evolution", joinColumns = { @JoinColumn(name = "id_pokemon")} , inverseJoinColumns = { @JoinColumn(name = "id_pokemon_evolution") })
	private Set<Pokemon> referencesTo;
	
	
	@ManyToMany(mappedBy="referencesTo")
    private Set<Pokemon> referencesFrom;
    public Pokemon init() {
        referencesTo = new HashSet<>();
        return this;
    }
	
}
