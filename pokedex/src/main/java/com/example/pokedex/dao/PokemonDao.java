package com.example.pokedex.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.pokedex.model.Pokemon;


public interface PokemonDao extends JpaRepository<Pokemon,Long> {
	
	Pokemon getByIdPokedexOrderByIdPokedexAsc (Long id);
	
	//Pokemon findByNameLikeIgnoreCase (String name);
	List<Pokemon> findByNameLikeIgnoreCase (String name);
	
	List<Pokemon> findAllByOrderByIdPokedexAsc ();
}
