package com.example.pokedex;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.pokedex.controller.PokemonFavouriteController;


@RunWith(SpringRunner.class)
@TestPropertySource(properties = {
		"spring.datasource.driver-class-name=org.postgresql.Driver",
		"spring.datasource.username=mpawyvpzauuoll",
		"spring.datasource.password=ec59719a8dcb16da8d94e5c344c94f4c4aacb82bd79745d454baa8af9b08b726",
		"spring.datasource.url=jdbc:postgresql://ec2-34-255-134-200.eu-west-1.compute.amazonaws.com:5432/d8mmkmj9t3j4hv"
})
@SpringBootTest
class PokedexApplicationTests {
	
	@Autowired
	private ApplicationContext context;

	@Test
	void contextLoads() {
		PokemonFavouriteController sc = (PokemonFavouriteController)context.getBean(PokemonFavouriteController.class);
		Assert.assertNotNull(sc);
		
	}

}
