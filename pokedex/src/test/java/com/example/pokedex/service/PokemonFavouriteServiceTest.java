package com.example.pokedex.service;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.pokedex.dao.MemberDao;
import com.example.pokedex.dao.PokemonDao;
import com.example.pokedex.dao.RelMemberPokemonFavDao;
import com.example.pokedex.model.Member;
import com.example.pokedex.model.Pokemon;

@RunWith(SpringRunner.class)
public class PokemonFavouriteServiceTest {

	
	@TestConfiguration
	static class PokemonFavouriteServiceTestContextConfiguration {

	@Bean
	public PokemonFavouriteService PokemonFavouriteService() {
		return new PokemonFavouriteServiceImpl();
	}
	
	@Autowired
	private PokemonFavouriteService pokemonFavouriteService;
	
	@MockBean
	private PokemonDao pokemonDao;
	
	@MockBean
	private MemberDao memberDao;
	
	@MockBean
	private RelMemberPokemonFavDao relMemberPokemonFavDao;
	
	@Before
	public void setUp() {

	}
	
	@Test
	public void saveFavourite() {
		Member member = new Member();
		member.setId(1l);
		member.setName("Veronica");
	
		Pokemon pokemon = new Pokemon();
		pokemon.setName("Squirtle");
		pokemon.setCombatPoints(946l);
		pokemon.setHealthPoints(112l);
		pokemon.setWeightFrom(0.44f);
		pokemon.setWeightTo(9.50f);
		pokemon.setHeightFrom(0.44f);
		pokemon.setHeightTo(0.44f);
		pokemon.setImageUrl("https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png");
		pokemon.setSoundUrl("https://play.pokemonshowdown.com/audio/cries/squirtle.ogg");
		pokemon.setIdPokedex(7l);

		Mockito.when(memberDao.getById(1L)).thenReturn(member);
		Mockito.when(pokemonDao.getByIdPokedexOrderByIdPokedexAsc(7l)).thenReturn(pokemon);	
	
		Assert.assertNotNull(pokemonFavouriteService.saveFavourite(pokemon.getIdPokedex(), member.getId()));

	}

	}
}
